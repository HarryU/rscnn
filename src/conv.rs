use crate::{pad::pad_with_zeros, Layer};
use ndarray::{s, Array, Array1, Array4, ArrayD, Axis, NewAxis};

pub struct Conv {
    weights: Array4<f64>,
    biases: ArrayD<f64>,
    d_weights: ArrayD<f64>,
    d_biases: ArrayD<f64>,
    previous: ArrayD<f64>,
}

impl Layer for Conv {
    fn forward_pass(&mut self, previous: &ArrayD<f64>) -> ArrayD<f64> {
        let weight_shape = self.weights.shape();
        let h_f = weight_shape[0];
        let w_f = weight_shape[1];
        let n_f = weight_shape[3];
        let h_pad = (h_f - 1) / 2;
        let w_pad = (w_f - 1) / 2;

        let previous_padded = pad_with_zeros(&previous, vec![0, h_pad, w_pad, 0]);

        let mut output = {
            let mut output_dim = previous.raw_dim();
            output_dim[3] = self.weights.shape()[3];
            Array::zeros(output_dim)
        };

        for f in 0..n_f {
            output.slice_mut(s![0, .., .., f]).assign(
                &(ndarray::Zip::from(
                    previous_padded
                        .slice(s![0, .., .., ..])
                        .windows(self.weights.slice(s![.., .., .., f]).raw_dim()),
                )
                .map_collect(|w| (&self.weights * &w).sum()))
                .sum_axis(Axis(2)),
            );
        }
        self.previous = previous.clone();
        output + &self.biases
    }

    fn backward_pass(&mut self, da_current: &ArrayD<f64>) -> ArrayD<f64> {
        // da_current is same shape as forward output: [batch_size, h, w, n_filters]
        // output is same shape as forward input: [batch_size, h, w, c]
        let weight_shape = self.weights.shape();
        let h_f = weight_shape[0];
        let w_f = weight_shape[1];
        let da_current_shape = da_current.shape();
        let previous_shape = self.previous.shape();
        let batch_size = previous_shape[0];
        let h_pad = (h_f - 1) / 2;
        let w_pad = (w_f - 1) / 2;
        let pad_width = vec![0, h_pad, w_pad, 0];
        let mut padded_shape = self.previous.raw_dim();
        for (ax, (&ax_len, &pad)) in self.previous.shape().iter().zip(&pad_width).enumerate() {
            padded_shape[ax] = ax_len + pad + pad;
        }
        let mut output = ArrayD::zeros(padded_shape);
        let previous_padded = pad_with_zeros(&self.previous, pad_width);

        for i in 0..da_current_shape[1] {
            for j in 0..da_current_shape[2] {
                let mut slice = output.slice_mut(s![.., i..i + h_f, j..j + w_f, ..]);
                slice += &((&self.weights.slice(s![NewAxis, .., .., .., ..])
                    * &da_current.slice(s![.., i..i + 1, j..j + 1, NewAxis, ..]))
                    .sum_axis(Axis(4)));
                self.d_weights +=
                    &((&previous_padded.slice(s![.., i..i + h_f, j..j + w_f, .., NewAxis])
                        * &da_current.slice(s![.., i..i + 1, j..j + 1, NewAxis, ..]))
                        .sum_axis(Axis(0)));
            }
        }
        self.d_biases = da_current
            .sum_axis(Axis(0))
            .sum_axis(Axis(0))
            .sum_axis(Axis(0))
            / batch_size as f64;
        self.d_weights /= batch_size as f64;
        output
            .slice_move(s![
                ..,
                h_pad..h_pad + previous_shape[1],
                w_pad..w_pad + previous_shape[2],
                ..
            ])
            .into_dyn()
    }
}

impl Conv {
    pub fn new(k_size: usize, n_filters: usize) -> Self {
        Conv {
            weights: Array4::<f64>::ones((k_size, k_size, 1, n_filters)),
            d_weights: Array4::<f64>::zeros((k_size, k_size, 1, n_filters)).into_dyn(),
            biases: Array1::<f64>::from_elem(n_filters, 1.0).into_dyn(),
            d_biases: Array1::<f64>::zeros(n_filters).into_dyn(),
            previous: Array4::<f64>::zeros((k_size, k_size, 1, n_filters)).into_dyn(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn forward_pass_same_shape() {
        let image = Array4::<f64>::ones((1, 64, 64, 1)).into_dyn();
        let mut c = Conv::new(3, 3);
        let o = c.forward_pass(&image);
        assert_eq!(&[1, 64, 64, 1], o.shape());
    }

    #[test]
    fn backward_pass_same_shape() {
        let image = Array4::<f64>::ones((1, 64, 64, 1)).into_dyn();
        let mut c = Conv::new(3, 3);
        let o = c.forward_pass(&image);
        let d = c.backward_pass(&o);
        assert_eq!(&[1, 64, 64, 1], d.shape());
    }
}
