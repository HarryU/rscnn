use ndarray::{ArrayD, Zip};
extern crate num;
use num::FromPrimitive;

use crate::Layer;

pub struct Relu<A> {
    z: ArrayD<A>,
}

impl Layer for Relu<f64> {
    fn forward_pass(&mut self, previous: &ArrayD<f64>) -> ArrayD<f64> {
        self.z = previous.mapv(|v| {
            if v < FromPrimitive::from_i32(0).unwrap() {
                FromPrimitive::from_i32(0).unwrap()
            } else {
                v
            }
        });
        self.z.clone()
    }

    fn backward_pass(&mut self, da_current: &ArrayD<f64>) -> ArrayD<f64> {
        let mut dz = da_current.clone();
        Zip::from(&mut dz).and(&self.z).for_each(|d, z| {
            if *z > FromPrimitive::from_i32(0).unwrap() {
                *d = d.clone();
            } else {
                *d = FromPrimitive::from_i32(0).unwrap()
            }
        });
        dz
    }
}

impl Relu<f64> {
    pub fn new(shape: &[usize]) -> Self {
        Relu {
            z: ArrayD::<f64>::zeros(shape),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn forward_pass_same_shape() {
        use ndarray::Array4;
        let a = Array4::<f64>::zeros((1, 3, 3, 1)).into_dyn();
        let mut r = Relu::<f64>::new(&[1, 3, 3, 1]);
        assert_eq!(r.forward_pass(&a).shape(), &[1, 3, 3, 1]);
    }

    #[test]
    fn forward_pass_no_non_zero() {
        use ndarray::arr2;
        let a = arr2(&[[-1.0, 2.0], [1.0, -3.0]]).into_dyn();
        let mut r = Relu::<f64>::new(&[2, 2]);
        assert_eq!(
            r.forward_pass(&a),
            arr2(&[[0.0, 2.0], [1.0, 0.0]]).into_dyn()
        );
    }
}
