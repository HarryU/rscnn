use ndarray::{Array, ArrayBase, Axis, Data, Dimension, Slice};
use num_traits::Zero;

pub fn pad_with_zeros<A, S, D>(arr: &ArrayBase<S, D>, pad_width: Vec<usize>) -> Array<A, D>
where
    A: Clone + Zero + std::fmt::Debug,
    S: Data<Elem = A>,
    D: Dimension,
{
    assert_eq!(
        arr.ndim(),
        pad_width.len(),
        "Array ndim must match length of `pad_width`."
    );

    // Compute shape of final padded array.
    let mut padded_shape = arr.raw_dim();
    for (ax, (&ax_len, &pad)) in arr.shape().iter().zip(&pad_width).enumerate() {
        padded_shape[ax] = ax_len + pad + pad;
    }

    let mut padded = Array::zeros(padded_shape);
    {
        // Select portion of padded array that needs to be copied from the
        // original array.
        let mut orig_portion = padded.view_mut();
        for (ax, &pad) in pad_width.iter().enumerate() {
            if pad != 0 {
                orig_portion
                    .slice_axis_inplace(Axis(ax), Slice::from(pad as isize..-(pad as isize)));
            } else {
                orig_portion.slice_axis_inplace(Axis(ax), Slice::from(..));
            }
        }
        // Copy the data from the original array.
        orig_portion.assign(arr);
    }
    padded
}
