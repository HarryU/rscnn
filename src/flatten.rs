use ndarray::{s, Array1, ArrayD, NewAxis};

use crate::Layer;

pub struct Flatten {
    shape: Option<Vec<usize>>,
}

impl Layer for Flatten {
    fn forward_pass(&mut self, previous: &ArrayD<f64>) -> ArrayD<f64> {
        self.shape = Some(previous.shape().to_vec());
        Array1::<f64>::from_iter(previous.iter().cloned())
            .slice_move(s![NewAxis, ..])
            .into_dyn()
    }

    fn backward_pass(&mut self, da_current: &ArrayD<f64>) -> ArrayD<f64> {
        da_current
            .clone()
            .into_shape(self.shape.clone().unwrap())
            .unwrap()
            .into_dyn()
    }
}

impl Flatten {
    pub fn new() -> Self {
        Self { shape: None }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn forward_pass_same_shape() {
        use ndarray::IxDyn;
        let a = ArrayD::<f64>::zeros(IxDyn(&[1, 3, 3, 1]));
        let mut r = Flatten::new();
        assert_eq!(r.forward_pass(&a).shape(), &[9, 1]);
    }
}
