use ndarray::{Array4, ArrayD};

mod conv;
mod dense;
mod flatten;
pub mod pad;
mod relu;
mod softmax;

fn main() {
    let n_filters = 3;
    let n_classes = 2;
    let k_size = 3;
    let batch_size = 1;
    let image_h = 64;
    let image_w = 64;
    let image_c = 1;
    let image = Array4::<f64>::ones((batch_size, image_h, image_w, image_c)).into_dyn();

    let c1 = conv::Conv::new(k_size, n_filters);
    let r = relu::Relu::new(&[batch_size, image_h, image_w, image_c]);
    let f = flatten::Flatten::new();
    let d = dense::Dense::new(
        batch_size,
        n_classes,
        image_h * image_w * n_filters * batch_size,
    );
    let s = softmax::Softmax::new(batch_size, n_classes);
    let layers: Vec<Box<dyn Layer>> = vec![
        Box::new(c1),
        Box::new(r),
        Box::new(f),
        Box::new(d),
        Box::new(s),
    ];
    let mut model = Sequential { layers: layers };
    let output = model.forward_pass(image);
    println!("{:?}", model.backward_pass(output).shape())
}

pub trait Layer {
    fn forward_pass(self: &mut Self, previous: &ArrayD<f64>) -> ArrayD<f64>;
    fn backward_pass(&mut self, da_current: &ArrayD<f64>) -> ArrayD<f64>;
}

struct Sequential {
    layers: Vec<Box<dyn Layer>>,
}

impl Sequential {
    fn forward_pass(&mut self, input_tensor: ArrayD<f64>) -> ArrayD<f64> {
        let mut previous = input_tensor;
        for layer in self.layers.iter_mut() {
            previous = layer.forward_pass(&previous);
        }
        previous
    }

    fn backward_pass(&mut self, input_tensor: ArrayD<f64>) -> ArrayD<f64> {
        let mut previous = input_tensor;
        for layer in self.layers.iter_mut().rev() {
            previous = layer.backward_pass(&previous);
        }
        previous
    }
}
