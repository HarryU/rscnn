use ndarray::{s, Array2, ArrayD, Axis, Ix2, NewAxis};

use crate::Layer;

pub(crate) struct Dense {
    weights: Array2<f64>,
    biases: Array2<f64>,
    d_weights: Array2<f64>,
    d_biases: Array2<f64>,
    previous: Array2<f64>,
}

impl Layer for Dense {
    fn forward_pass(&mut self, previous: &ArrayD<f64>) -> ArrayD<f64> {
        self.previous = previous
            .clone()
            .into_dimensionality::<Ix2>()
            .unwrap()
            .clone();
        (&self.previous.dot(&self.weights.t()) + &self.biases).into_dyn()
    }

    fn backward_pass(&mut self, da_current: &ArrayD<f64>) -> ArrayD<f64> {
        let n = self.previous.shape()[0] as f64;
        self.d_weights = da_current
            .clone()
            .into_dimensionality::<Ix2>()
            .unwrap()
            .t()
            .dot(&self.previous)
            / n;
        self.d_biases = da_current.sum_axis(Axis(0)).slice_move(s![NewAxis, ..]) / n;
        da_current
            .clone()
            .into_dimensionality::<Ix2>()
            .unwrap()
            .dot(&self.weights)
            .into_dyn()
    }
}

impl Dense {
    pub fn new(batch_size: usize, n_current: usize, n_previous: usize) -> Self {
        // n_current is number of output neurons
        // n_previous is number of input neurons
        Self {
            weights: Array2::<f64>::zeros((n_current, n_previous)),
            d_weights: Array2::<f64>::zeros((n_current, n_previous)),
            biases: Array2::<f64>::zeros((1, n_current)),
            d_biases: Array2::<f64>::zeros((1, n_current)),
            previous: Array2::<f64>::zeros((batch_size, n_previous)),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn forward_pass_same_shape() {
        let a = Array2::<f64>::zeros((1, 3)).into_dyn();
        let mut r = Dense::new(1, 3, 3);
        assert_eq!(r.forward_pass(&a).shape(), &[1, 3]);
    }

    #[test]
    fn forward_pass_all_zero() {
        // only all zero because initial w and b are zero for now
        use ndarray::arr2;
        let a = arr2(&[[-1.0, 2.0, 1.0]]).into_dyn();
        let mut r = Dense::new(1, 3, 3);
        assert_eq!(r.forward_pass(&a), arr2(&[[0.0, 0.0, 0.0]]).into_dyn());
    }

    #[test]
    fn backward_pass_same_shape() {
        let a = Array2::<f64>::zeros((1, 3)).into_dyn();
        let mut r = Dense::new(1, 3, 3);
        assert_eq!(r.backward_pass(&a).shape(), &[1, 3]);
    }

    #[test]
    fn backward_pass_all_zero() {
        // only all zero because initial w and b are zero for now
        use ndarray::arr2;
        let a = arr2(&[[-1.0, 2.0, 1.0]]).into_dyn();
        let mut r = Dense::new(1, 3, 3);
        assert_eq!(r.backward_pass(&a), arr2(&[[0.0, 0.0, 0.0]]).into_dyn());
    }
}
