use ndarray::{s, Array2, ArrayD, Axis, NewAxis};
use ndarray_stats::QuantileExt;

use crate::Layer;

pub(crate) struct Softmax {
    z: ArrayD<f64>,
}

impl Layer for Softmax {
    fn forward_pass(&mut self, previous: &ArrayD<f64>) -> ArrayD<f64> {
        let max_previous = previous
            .map_axis(Axis(1), |a| *(a.max().unwrap()))
            .slice_move(s![.., NewAxis]);
        let e = (previous - &max_previous).mapv(f64::exp);
        let e_sum = e.sum_axis(Axis(1)).slice_move(s![.., NewAxis]).into_dyn();
        self.z = e / e_sum;
        self.z.clone()
    }

    fn backward_pass(&mut self, da_current: &ArrayD<f64>) -> ArrayD<f64> {
        da_current.clone()
    }
}

impl Softmax {
    pub fn new(batch_size: usize, n_classes: usize) -> Self {
        Self {
            z: Array2::zeros((batch_size, n_classes)).into_dyn(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn forward_pass_same_shape() {
        let a = Array2::<f64>::zeros((1, 3)).into_dyn();
        let mut s = Softmax::new(1, 3);
        assert_eq!(s.forward_pass(&a).shape(), &[1, 3]);
    }

    #[test]
    fn forward_pass_all_zero() {
        // only all zero because initial w and b are zero for now
        use ndarray::arr2;
        let a = arr2(&[[1.0, 1.0]]).into_dyn();
        let mut s = Softmax::new(1, 2);
        assert_eq!(s.forward_pass(&a), arr2(&[[0.5, 0.5]]).into_dyn());
    }
}
